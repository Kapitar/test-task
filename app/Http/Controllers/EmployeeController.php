<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{   

    /**
     * Getting fillial data
     
     * @return array filials
    */
    public function getFilials() {
        $fillials = DB::table('filial')->get();
        return response()->json($fillials, 200);
    }

    /**
     * Getting employees data
     
     * @return array employees
    */
    public function getEmployees() {
        $employees = DB::table('employees')->get();
        return response()->json($employees, 200);
    }
}
